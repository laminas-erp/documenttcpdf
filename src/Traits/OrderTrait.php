<?php

namespace Lerp\DocumentTcpdf\Traits;

use Lerp\Order\Entity\Order\OrderItemEntity;

trait OrderTrait
{

    /**
     * @param OrderItemEntity[] $orderItemEntities
     */
    protected array $orderItemEntities;

    /**
     * @param OrderItemEntity[] $orderItemEntities
     */
    public function setOrderItemEntities(array $orderItemEntities): void
    {
        $this->orderItemEntities = $orderItemEntities;
    }

    /**
     * @return OrderItemEntity[]
     */
    public function getOrderItemEntities(): array
    {
        return $this->orderItemEntities;
    }

}
