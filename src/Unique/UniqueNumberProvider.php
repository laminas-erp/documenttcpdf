<?php

namespace Lerp\DocumentTcpdf\Unique;

use Laminas\Log\Logger;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Traits\OfferDocTrait;
use Lerp\Document\Traits\OrderDocTrait;
use Lerp\Document\Traits\PurchaseDocTrait;
use Lerp\Document\Unique\UniqueNumberProviderInterface;

/**
 * Using:
 * 1. init()
 * 2. generate(DocumentService::DOC_TYPE_*)
 * 3. getNumberComplete()
 *
 * Using quickly:
 * 1. computeGetNumberComplete(DocumentService::DOC_TYPE_*)
 */
class UniqueNumberProvider implements UniqueNumberProviderInterface
{
    use OrderDocTrait;
    use PurchaseDocTrait;
    use OfferDocTrait;

    const PAD_LENGTH = 4;
    const PAD_STRING = '0';

    /**
     * self::PAD_LENGTH plus stuff
     */
    const UNIQUE_LENGTH = 8;
    protected Logger $logger;

    /**
     * Standort Nummer
     */
    const PREFIX_NUMBER = 9;
    protected int $year;
    protected int $yearShort;
    protected \DateTime $dateTimeFrom;
    protected \DateTime $dateTimeTo;
    protected int $number;
    protected string $numberComplete;
    protected string $message;
    protected DocumentService $documentService;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        $this->year = date('Y');
        $this->yearShort = substr($this->year, 2, 2);
        $this->dateTimeFrom = new \DateTime($this->year . '-01-01 00:00:00');
        $this->dateTimeTo = new \DateTime($this->year . '-12-31 23:59:59');
    }

    /**
     * @param string $docType On of the DocumentService()->$docTypes
     * @return bool
     */
    public function generate(string $docType): bool
    {
        if (!in_array($docType, $this->documentService->getDocTypes())) {
            $this->message = 'Document type is not supported: ' . $docType;
            return false;
        }
        if (
            !isset($this->dateTimeFrom) || !$this->dateTimeFrom instanceof \DateTime
            || !isset($this->dateTimeTo) || !$this->dateTimeTo instanceof \DateTime
        ) {
            $this->message = 'Dates are not set. Perhaps you forgot to call the init() method!?';
            return false;
        }
        switch ($docType) {
            case DocumentService::DOC_TYPE_PURCHASEORDER:
                if (($this->number = $this->docPurchaseOrderTable->getMaxDocPurchaseOrderNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-purchaseOrder number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_PURCHASEORDER_DELIVERY:
                if (($this->number = $this->docPurchaseDeliverTable->getMaxDocPurchaseDeliverNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-purchaseDeliver number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_PURCHASEORDER_REQUEST:
                if (($this->number = $this->docPurchaseRequestTable->getMaxDocPurchaseRequestNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-purchaseRequest number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_OFFER:
                if (($this->number = $this->docOfferTable->getMaxDocOfferNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-offer number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ESTIMATE:
                if (($this->number = $this->docEstimateTable->getMaxDocEstimateNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-estimate number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ORDER_CONFIRM:
                if (($this->number = $this->docOrderConfirmTable->getMaxDocOrderConfirmNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-orderConfirm number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_INVOICE:
                if (($this->number = $this->docInvoiceTable->getMaxDocInvoiceNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-invoice number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_PROFORMA:
                if (($this->number = $this->docProformaTable->getMaxDocProformaNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-proforma number';
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ORDER_DELIVERY:
                if (($this->number = $this->docDeliveryTable->getMaxDocDeliveryNoInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
                    $this->message = 'Error while compute max-doc-delivery number';
                    return false;
                }
                break;
        }
        $this->number++;
        return $this->computeGlue();
    }

    protected function computeGlue(): bool
    {
        if (empty($this->yearShort) || empty($this->number)) {
            return false;
        }
        $this->numberComplete = $this->yearShort . '-' . self::PREFIX_NUMBER
            . str_pad($this->number, self::PAD_LENGTH, self::PAD_STRING, STR_PAD_LEFT);
        return strlen($this->numberComplete) == self::UNIQUE_LENGTH;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getNumberComplete(): string
    {
        return $this->numberComplete;
    }

    /**
     * @param string $docType On of the DocumentService()->$docTypes
     * @return string
     */
    public function computeGetNumberComplete(string $docType): string
    {
        try {
            $this->init();
        } catch (\Exception $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $e->getMessage());
            $this->message = 'Error while init()';
            return '';
        }
        if (!$this->generate($docType)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        return $this->numberComplete;
    }

    /**
     * @return string If $this->generate() returns false then there will be a message.
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
