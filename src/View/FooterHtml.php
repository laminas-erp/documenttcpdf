<?php

namespace Lerp\DocumentTcpdf\View;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class FooterHtml extends AbstractViewHelper
{
    const TEMPLATE = 'lerpDocumentTcpdf/footerHtml';

    protected string $brandColor;

    public function setBrandColor(string $brandColor): void
    {
        $this->brandColor = $brandColor;
    }

    /**
     * @return string
     */
    public function __invoke(): string
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('colorBrand', $this->brandColor);
        return $this->getView()->render($viewModel);
    }
}
