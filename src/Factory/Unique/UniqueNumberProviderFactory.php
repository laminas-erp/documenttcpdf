<?php

namespace Lerp\DocumentTcpdf\Factory\Unique;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Offer\DocOfferTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;
use Lerp\DocumentTcpdf\Unique\UniqueNumberProvider;

class UniqueNumberProviderFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $unique = new UniqueNumberProvider();
        $unique->setLogger($container->get('logger'));
        // OrderDocTrait
        $unique->setDocEstimateTable($container->get(DocEstimateTable::class));
        $unique->setDocOrderConfirmTable($container->get(DocOrderConfirmTable::class));
        $unique->setDocDeliveryTable($container->get(DocDeliveryTable::class));
        $unique->setDocInvoiceTable($container->get(DocInvoiceTable::class));
        $unique->setDocProformaTable($container->get(DocProformaTable::class));
        // PurchaseDocTrait
        $unique->setDocPurchaseOrderTable($container->get(DocPurchaseOrderTable::class));
        $unique->setDocPurchaseDeliverTable($container->get(DocPurchaseDeliverTable::class));
        $unique->setDocPurchaseRequestTable($container->get(DocPurchaseRequestTable::class));
        // OfferDocTrait
        $unique->setDocOfferTable($container->get(DocOfferTable::class));

        $unique->setDocumentService($container->get(DocumentService::class));
        return $unique;
    }
}
