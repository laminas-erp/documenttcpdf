<?php

namespace Lerp\DocumentTcpdf\Factory\Pdf;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;
use Lerp\DocumentTcpdf\View\FooterHtml;

class PdfCorrespondenceDefaultFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $hf = new PdfCorrespondenceDefault();
        $config = $container->get('config');
        $configDoc = $config['lerp_document'];
        $configTcpdf = $config['lerp_document_tcpdf'];
        $hf->setProtectPasswd($configTcpdf['protect_passwd']);
        $hf->setStorageLocationImg($configTcpdf['path_img']);
        $hf->setStorageLocationPdf($configDoc['path_pdf']);
        $hf->setDocumentTranslator($container->get('DocumentTranslator'));
        $hf->setColorArrayBrand($configTcpdf['brand_rgb']);
        $hf->setPathLogo($configTcpdf['image_paths']['logo']);
        $hf->setPathBrand($configTcpdf['image_paths']['brand']);
        $hf->setPathLogoSimple($configTcpdf['image_paths']['logo_simple']);
        $hf->SetAuthor($configTcpdf['data_author']);
        $hf->setUserdataAddressLineOrigin($configTcpdf['address_line_origin']);
        /** @var HelperPluginManager $pluginManager */
        $pluginManager = $container->get('ViewHelperManager');
        /** @var FooterHtml $x */
        $footerHtml = $pluginManager->get(FooterHtml::class);
        $hf->setUserdataFooterHtml($footerHtml());
        return $hf;
    }
}
