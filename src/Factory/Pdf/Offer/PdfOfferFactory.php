<?php

namespace Lerp\DocumentTcpdf\Factory\Pdf\Offer;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\Offer\PdfOffer;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;

class PdfOfferFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $hf = new PdfOffer();
        $hf->setDocumentService($container->get(DocumentService::class));
        $hf->setDocumentTranslator($container->get('DocumentTranslator'));
        $hf->setNumberFormatService($container->get(NumberFormatService::class));
        $hf->setPdf($container->get(PdfCorrespondenceDefault::class));
        return $hf;
    }
}
