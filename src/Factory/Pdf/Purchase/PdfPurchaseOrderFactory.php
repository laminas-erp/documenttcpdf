<?php

namespace Lerp\DocumentTcpdf\Factory\Pdf\Purchase;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;
use Lerp\DocumentTcpdf\Pdf\Purchase\PdfPurchaseOrder;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PdfPurchaseOrderFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $hf = new PdfPurchaseOrder();
        $hf->setDocumentService($container->get(DocumentService::class));
        $hf->setDocumentTranslator($container->get('DocumentTranslator'));
        $hf->setNumberFormatService($container->get(NumberFormatService::class));
        $hf->setPdf($container->get(PdfCorrespondenceDefault::class));
        return $hf;
    }
}
