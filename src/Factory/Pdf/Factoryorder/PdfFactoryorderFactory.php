<?php

namespace Lerp\DocumentTcpdf\Factory\Pdf\Factoryorder;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\Factoryorder\PdfFactoryorder;

class PdfFactoryorderFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $hf = new PdfFactoryorder();
        $hf->setDocumentService($container->get(DocumentService::class));
        $hf->setDocumentTranslator($container->get('DocumentTranslator'));
        $hf->setNumberFormatService($container->get(NumberFormatService::class));
        $config = $container->get('config');
        $configDoc = $config['lerp_document'];
        $configTcpdf = $config['lerp_document_tcpdf'];
        $hf->setProtectPasswd($configTcpdf['protect_passwd']);
        $hf->setStorageLocationTmp($configTcpdf['path_tmp']);
        $hf->setStorageLocationImg($configTcpdf['path_img']);
        $hf->setStorageLocationPdf($configDoc['path_pdf']);
        $hf->setColorArrayBrand($configTcpdf['brand_rgb']);
        $hf->setPathLogo($configTcpdf['image_paths']['logo']);
        $hf->setPathBrand($configTcpdf['image_paths']['brand']);
        $hf->setPathLogoSimple($configTcpdf['image_paths']['logo_simple']);
        $hf->SetAuthor($configTcpdf['data_author']);
        return $hf;
    }
}
