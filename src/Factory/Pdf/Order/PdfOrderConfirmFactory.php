<?php

namespace Lerp\DocumentTcpdf\Factory\Pdf\Order;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\Order\PdfOrderConfirm;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;

class PdfOrderConfirmFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $hf = new PdfOrderConfirm();
        $hf->setDocumentService($container->get(DocumentService::class));
        $hf->setDocumentTranslator($container->get('DocumentTranslator'));
        $hf->setNumberFormatService($container->get(NumberFormatService::class));
        $hf->setPdf($container->get(PdfCorrespondenceDefault::class));
        return $hf;
    }
}
