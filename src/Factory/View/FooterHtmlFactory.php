<?php

namespace Lerp\DocumentTcpdf\Factory\View;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\DocumentTcpdf\View\FooterHtml;

class FooterHtmlFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new FooterHtml();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setBrandColor($container->get('config')['lerp_document_tcpdf']['brand_color']);
        return $viewHelper;
    }
}
