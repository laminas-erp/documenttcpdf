<?php

namespace Lerp\DocumentTcpdf\Pdf\Purchase;

use Lerp\Document\Pdf\Concrete\PurchaseOrderProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\Purchase\Entity\PurchaseOrderItemEntity;

abstract class AbstractPdfPurchaseOrder extends AbstractDocumentProvider implements PurchaseOrderProviderInterface
{
    /**
     * @var PurchaseOrderItemEntity[]
     */
    protected array $purchaseOrderItemEntities;
    protected string $purchaseOrderNoCompl;
    protected string $docPurchaseOrderNoCompl;

    /**
     * @param PurchaseOrderItemEntity[] $purchaseOrderItemEntities
     */
    public function setPurchaseOrderItemEntities(array $purchaseOrderItemEntities): void
    {
        $this->purchaseOrderItemEntities = $purchaseOrderItemEntities;
    }

    public function getPurchaseOrderItemEntities(): array
    {
        return $this->purchaseOrderItemEntities;
    }

    public function setPurchaseOrderNoCompl(string $purchaseOrderNoCompl): void
    {
        $this->purchaseOrderNoCompl = $purchaseOrderNoCompl;
    }

    public function setDocPurchaseOrderNoCompl(string $docPurchaseOrderNoCompl): void
    {
        $this->docPurchaseOrderNoCompl = $docPurchaseOrderNoCompl;
    }
}
