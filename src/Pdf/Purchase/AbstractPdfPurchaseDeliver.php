<?php

namespace Lerp\DocumentTcpdf\Pdf\Purchase;

use Lerp\Document\Pdf\Concrete\PurchaseDeliverProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\Purchase\Entity\PurchaseOrderItemAttachEntity;

abstract class AbstractPdfPurchaseDeliver extends AbstractDocumentProvider implements PurchaseDeliverProviderInterface
{
    protected string $purchaseOrderNoCompl;
    protected string $docPurchaseDeliverNoCompl;

    public function setPurchaseOrderNoCompl(string $purchaseOrderNoCompl): void
    {
        $this->purchaseOrderNoCompl = $purchaseOrderNoCompl;
    }

    public function setDocPurchaseDeliverNoCompl(string $docPurchaseDeliverNoCompl): void
    {
        $this->docPurchaseDeliverNoCompl = $docPurchaseDeliverNoCompl;
    }

    /**
     * @var PurchaseOrderItemAttachEntity[]
     */
    protected array $purchaseOrderItemAttachEntities;

    /**
     * @param PurchaseOrderItemAttachEntity[] $purchaseOrderItemAttachEntities
     */
    public function setPurchaseOrderItemAttachEntities(array $purchaseOrderItemAttachEntities): void
    {
        $this->purchaseOrderItemAttachEntities = $purchaseOrderItemAttachEntities;
    }

    /**
     * @return PurchaseOrderItemAttachEntity[]
     */
    public function getPurchaseOrderItemAttachEntities(): array
    {
        return $this->purchaseOrderItemAttachEntities;
    }

}
