<?php

namespace Lerp\DocumentTcpdf\Pdf\Purchase;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\PdfClass;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;

class PdfPurchaseRequest extends AbstractPdfPurchaseRequest
{
    protected PdfCorrespondenceDefault $pdf;

    public function setPdf(PdfCorrespondenceDefault $pdf): void
    {
        $this->pdf = $pdf;

    }

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void
    {
        $this->baseDataEntity = $baseDataEntity;
        $this->pdf->setBaseDataEntity($this->baseDataEntity);
    }

    public function makeDocument(): void
    {
        if (empty($this->purchaseRequestItemEntities)) {
            return;
        }
        $this->pdf->makeBasics();
        $purchaseRequest = $this->purchaseRequestItemEntities[0];
        $this->pdfFilename = time() . '_' . DocumentService::FILENAME_PURCHASEORDER_REQUEST;
        $this->filepathPdf = $this->documentService->computeDocumentFolder(
                $this->pdf->getStorageLocationPdf(),
                DocumentService::DOC_TYPE_PURCHASEORDER_REQUEST,
                floor($purchaseRequest->getPurchaseRequestTimeCreateUnix()),
                $purchaseRequest->getPurchaseRequestUuid()
            ) . DIRECTORY_SEPARATOR . $this->pdfFilename;
        $noString = $this->purchaseRequestNoCompl . '/' . $this->docPurchaseRequestNoCompl;
        $this->baseDataEntity->setInfoboxPurchaseRequestNo($this->purchaseRequestNoCompl);

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), 'B');
        $this->pdf->AddPage('P');
        $this->pdf->Cell(25, 0, $this->documentTranslator->translate('purchaserequest_no', 'lerp_doc') . ' ' . $noString, 0, 1, 'L', false); // $purchaseRequest->getPurchaseRequestNo()

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->Ln();
        $this->pdf->Cell(25, 0, $this->baseDataEntity->getSalutation() . ',', 0, 1, 'L', false);
        $this->pdf->Ln();
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentStartHTML(), 0, 1);
        $this->pdf->Ln();

        $this->pdf->Row5header(
            $this->documentTranslator->translate('th_pos', 'lerp_doc')
            , $this->documentTranslator->translate('th_amount', 'lerp_doc')
            , $this->documentTranslator->translate('th_quantityunit', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_no', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_desc', 'lerp_doc')
        );

        foreach ($this->purchaseRequestItemEntities as $item) {
            $this->pdf->Row5(
                $item->getPurchaseRequestItemId(),
                $this->numberFormatService->format($item->getPurchaseRequestItemQuantity()),
                $item->getQuantityunitLabel(),
                $item->getProductNoNo(),
                $item->getPurchaseRequestItemTextShort());
        }
        $this->pdf->Ln();

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentEndHTML(), 0, 1);
    }

    /**
     * @return string
     * After the call of this function, $this holds ONLY members from base class TCPDF (all other are lost)!
     */
    public function writeDocument(): string
    {
        $this->pdf->Output($p = $this->filepathPdf, 'F');
        /*
         * echt krank: laut debugger ist man ab hier immernoch in PdfOrderConfirm.
         * Es existieren aber keine Klassenvariablen aus PdfClass, PdfHeaderFooterBrand, PdfCorrespondenceDefault oder PdfOrderConfirm.
         * Es existieren nur Klassenvariablen aus TCPDF
         */
        return $p;
    }
}
