<?php

namespace Lerp\DocumentTcpdf\Pdf\Purchase;

use Lerp\Document\Pdf\Concrete\PurchaseRequestProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\Purchase\Entity\PurchaseRequestItemEntity;

abstract class AbstractPdfPurchaseRequest extends AbstractDocumentProvider implements PurchaseRequestProviderInterface
{
    /**
     * @var PurchaseRequestItemEntity[]
     */
    protected array $purchaseRequestItemEntities;
    protected string $purchaseRequestNoCompl;
    protected string $docPurchaseRequestNoCompl;

    /**
     * @param PurchaseRequestItemEntity[] $purchaseRequestItemEntities
     */
    public function setPurchaseRequestItemEntities(array $purchaseRequestItemEntities): void
    {
        $this->purchaseRequestItemEntities = $purchaseRequestItemEntities;
    }

    /**
     * @return PurchaseRequestItemEntity[]
     */
    public function getPurchaseRequestItemEntities(): array
    {
        return $this->purchaseRequestItemEntities;
    }

    public function setPurchaseRequestNoCompl(string $purchaseRequestNoCompl): void
    {
        $this->purchaseRequestNoCompl = $purchaseRequestNoCompl;
    }

    public function setDocPurchaseRequestNoCompl(string $docPurchaseRequestNoCompl): void
    {
        $this->docPurchaseRequestNoCompl = $docPurchaseRequestNoCompl;
    }
}
