<?php

namespace Lerp\DocumentTcpdf\Pdf\Purchase;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\PdfClass;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;

class PdfPurchaseDeliver extends AbstractPdfPurchaseDeliver
{
    protected PdfCorrespondenceDefault $pdf;


    public function setPdf(PdfCorrespondenceDefault $pdf): void
    {
        $this->pdf = $pdf;
    }

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void
    {
        $this->baseDataEntity = $baseDataEntity;
        $this->pdf->setBaseDataEntity($this->baseDataEntity);
    }

    public function makeDocument(): void
    {
        if (empty($this->purchaseOrderItemAttachEntities)) {
            return;
        }
        $this->pdf->makeBasics();
        $purchaseOrder = $this->purchaseOrderItemAttachEntities[0];
        $this->pdfFilename = time() . '_' . DocumentService::FILENAME_PURCHASEORDER_DELIVERY;
        $this->filepathPdf = $this->documentService->computeDocumentFolder(
                $this->pdf->getStorageLocationPdf(),
                DocumentService::DOC_TYPE_PURCHASEORDER_DELIVERY,
                floor($purchaseOrder->getPurchaseOrderTimeCreateUnix()),
                $purchaseOrder->getPurchaseOrderUuid()
            ) . DIRECTORY_SEPARATOR . $this->pdfFilename;
        $noString = $this->purchaseOrderNoCompl . '/' . $this->docPurchaseDeliverNoCompl;
        $this->baseDataEntity->setInfoboxPurchaseOrderNo($this->purchaseOrderNoCompl);

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), 'B');
        $this->pdf->AddPage('P');
        $this->pdf->Cell(25, 0, $this->documentTranslator->translate('delivery_no', 'lerp_doc') . ' ' . $noString, 0, 1, 'L', false);

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->Ln();
        $this->pdf->Cell(25, 0, $this->baseDataEntity->getSalutation() . ',', 0, 1, 'L', false);
        $this->pdf->Ln();
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentStartHTML(), 0, 1);
        $this->pdf->Ln();

        $this->pdf->Row4header(
            $this->documentTranslator->translate('th_amount', 'lerp_doc')
            , $this->documentTranslator->translate('th_quantityunit', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_no', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_desc', 'lerp_doc')
        );

        foreach ($this->purchaseOrderItemAttachEntities as $item) {
            $this->pdf->Row4(
                $this->numberFormatService->format($item->getPurchaseOrderItemAttachQuantity()),
                $item->getQuantityunitLabel(),
                $item->getProductNoNo(),
                $item->getPurchaseOrderItemAttachTextShort()
            );
        }
        $this->pdf->Ln();

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentEndHTML(), 0, 1);
    }

    /**
     * @return string
     * After the call of this function, $this holds ONLY members from base class TCPDF (all other are lost)!
     */
    public function writeDocument(): string
    {
        $this->pdf->Output($p = $this->filepathPdf, 'F');
        /*
         * echt krank: laut debugger ist man ab hier immernoch in PdfOrderConfirm.
         * Es existieren aber keine Klassenvariablen aus PdfClass, PdfHeaderFooterBrand, PdfCorrespondenceDefault oder PdfOrderConfirm.
         * Es existieren nur Klassenvariablen aus TCPDF
         */
        return $p;
    }
}
