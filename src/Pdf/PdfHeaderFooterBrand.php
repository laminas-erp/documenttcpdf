<?php

namespace Lerp\DocumentTcpdf\Pdf;

use Bitkorn\Images\Tools\Svg\SvgAspectRatio;
use Lerp\Document\Entity\BaseDataEntity;

class PdfHeaderFooterBrand extends PdfClass
{
    protected string $protectPasswd = '';
    protected string $fontFamilyDefault = 'helvetica';
    protected int $footerPositionFromBottom = -25;
    protected SvgAspectRatio $svgAspectRatio;

    protected int $positionTopmost = 6;
    protected int $contentStart = 26;
    protected $creator = 'Aviation ERP';

    /**
     * @var array From config
     */
    protected array $colorArrayBrand = [];

    /**
     * @var string From config
     */
    protected string $pathLogo = '';

    /**
     * @var string From config
     */
    protected string $pathBrand = '';

    /**
     * @var string From config
     */
    protected string $pathLogoSimple = '';

    protected int $logoWidth = 0;
    protected int $logoHeight = 20; // DIN 5008 Briefkopf minus 7
    protected int $brandWidth = 0;
    protected int $brandHeight = 20; // DIN 5008 Briefkopf minus 7
    protected int $logoSimpleWidth = 0;
    protected int $logoSimpleHeight = 12; // fix

    protected string $userdataAddressLineOrigin = '';
    protected string $userdataFooterText = '';
    protected string $userdataFooterHtml = '';
    protected BaseDataEntity $baseDataEntity;

    public function setProtectPasswd(string $protectPasswd): void
    {
        $this->protectPasswd = $protectPasswd;
    }

    public function getFontFamilyDefault(): string
    {
        return $this->fontFamilyDefault;
    }

    public function setColorArrayBrand(array $colorArrayBrand): void
    {
        $this->colorArrayBrand = $colorArrayBrand;
    }

    public function setPathLogo(string $pathLogo): void
    {
        $this->pathLogo = $pathLogo;
    }

    public function setPathBrand(string $pathBrand): void
    {
        $this->pathBrand = $pathBrand;
    }

    public function setPathLogoSimple(string $pathLogoSimple): void
    {
        $this->pathLogoSimple = $pathLogoSimple;
    }

    /**
     * Perhaps it is already set in the factory.
     * @param string $userdataAddressLineOrigin
     */
    public function setUserdataAddressLineOrigin(string $userdataAddressLineOrigin): void
    {
        $this->userdataAddressLineOrigin = $userdataAddressLineOrigin;
    }

    /**
     * Perhaps it is already set in the factory.
     * @param string $userdataFooterHtml
     */
    public function setUserdataFooterHtml(string $userdataFooterHtml): void
    {
        $this->userdataFooterHtml = $userdataFooterHtml;
    }

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void
    {
        $this->baseDataEntity = $baseDataEntity;
    }

    /**
     * 1. compute images width with aspect ratio (keep height)
     * 2. set PDF protection password
     * 3. set PDF meta data
     * 4. set color and font
     */
    public function makeBasics()
    {
        $this->svgAspectRatio = new SvgAspectRatio();
        if (!$this->svgAspectRatio->loadSvg($this->storageLocationImg . $this->pathLogo)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->storageLocationImg . $this->pathLogo);
        }
        $this->logoWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->logoHeight;

        if (!$this->svgAspectRatio->loadSvg($this->storageLocationImg . $this->pathBrand)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->storageLocationImg . $this->pathBrand);
        }
        $this->brandWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->brandHeight;

        if (!$this->svgAspectRatio->loadSvg($this->storageLocationImg . $this->pathLogoSimple)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->storageLocationImg . $this->pathLogoSimple);
        }
        $this->logoSimpleWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->logoSimpleHeight;

        $this->SetProtection(['modify'], null, $this->protectPasswd);
        // PDF basics
        $this->SetCreator($this->creator);
        $this->SetAuthor($this->author);
        $this->SetTitle($this->title);
        $this->SetSubject($this->subject);
        $this->SetMargins($this->marginLeft, $this->marginTop, $this->marginRight); // left, top, right
        $this->SetAutoPageBreak(true, $this->marginBottom + (-1 * $this->footerPositionFromBottom));

        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->SetFontSize($this->fontSizeInitial);

        $this->setCellPaddings(1, 1, 1, 1);
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);
        $this->SetY($this->contentStart);
    }

    public function Header()
    {
        if ($this->getPage() == 1) {
            $this->SetFontSize($this->fontSizeInitial);
            $this->ImageSVG($this->storageLocationImg . $this->pathLogo, ($this->getPageWidth() / 2) - ($this->logoWidth / 2), $this->positionTopmost, null, $this->logoHeight);
            $this->ImageSVG($this->storageLocationImg . $this->pathBrand, $this->getPageWidth() - $this->marginRight - $this->brandWidth, $this->positionTopmost, null, $this->brandHeight);

            $this->SetY($this->positionTopmost);
            $this->SetFillColorArray($this->colorArrayWhite);
            $this->SetTextColorArray($this->colorArrayBlack);
            $this->SetFont($this->fontFamilyDefault, 'B');
            $this->SetFontSize(PdfClass::FONT_SIZE_S);
            $this->Cell(100, 0, 'Please visit our new Website', 0, 0, 'L', false);
            $this->Ln();
            $this->SetFontSize(PdfClass::FONT_SIZE_M);
            $this->SetTextColorArray($this->colorArrayBrand);
            $this->Cell(100, 0, 'www.gomolzig.de', 0, 0, 'L', false, 'http://gomolzig.de');

            /*
             * Ruecksendeangabe
             * $this->Rect(20, 27, 85, 5);
             */
            $this->SetTextColorArray($this->colorArrayBlack);
            $this->SetFont($this->fontFamilyDefault, '');
            $this->SetFontSize(PdfClass::FONT_SIZE_XS);
            $this->SetY(27);
            $this->Cell(80, 5, $this->userdataAddressLineOrigin, 0, 0, 'L', false);
            /*
             * Zusatz und Vermerkzone
             * $this->Rect(20, 32, 85, 12);
             */
            /*
             * Anschriftzone
             * $this->Rect(20, 44, 85, 28);
             */
            $this->SetY(44);
            $this->SetFontSize(PdfClass::FONT_SIZE_M);
            $this->MultiCell(80, 0,
                $this->baseDataEntity->getRecipientLine1()
                . "\n" . $this->baseDataEntity->getRecipientLine2()
                . "\n" . $this->baseDataEntity->getRecipientLine3()
                . "\n" . $this->baseDataEntity->getRecipientLine4(),
                0, 'L', false, 1);

            /*
             * Infobox.Y + Infobox.height
             */
            $this->contentStart = 92;
        } else {
            $this->SetY($this->positionTopmost);
            $this->ImageSVG($this->storageLocationImg . $this->pathLogoSimple, ($this->getPageWidth() / 2) - ($this->logoSimpleWidth / 2), $this->positionTopmost, null, $this->logoSimpleHeight);
            $this->SetY($this->positionTopmost + $this->logoSimpleHeight + 1);
            $this->Cell($this->getContentWidth(), 1, '', 'T', 1, 'C', false);

            $this->contentStart = $this->positionTopmost + $this->logoSimpleHeight + 1;
        }
    }

    public function Footer()
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_XXS);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->footerPositionFromBottom - 5);
        $this->Cell(0, 0, $this->documentTranslator->translate('agb_hint', 'lerp_doc'), 0, 1, 'L', false);

        $this->writeHTMLCell($this->getContentWidth(), 0, $this->marginLeft, $this->footerPositionFromBottom, $this->userdataFooterHtml, 0, 1);

        $this->SetY($this->getPageHeight() - 6);
        $this->Cell($this->getContentWidth(), 0, $this->documentTranslator->translate('page', 'lerp_doc') . ' ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'C');
    }
}
