<?php

namespace Lerp\DocumentTcpdf\Pdf;

class PdfCorrespondenceDefault extends PdfHeaderFooterBrand
{

    /**
     * Create the header on page one.
     */
    public function Header()
    {
        parent::Header();

        if ($this->getPage() == 1) {
            /*
             * Informationsblock
             */
            $this->Rect(125, 32, 75, 60); // x=125; y=32; w=75 are ISO 5008
            $this->SetFontSize(PdfClass::FONT_SIZE_S);
            $x = 126;
            $this->SetXY($x, 33);
            $labelLength = 0;
            foreach ($this->baseDataEntity->getInfobox() as $key => $val) {
                if (empty($this->baseDataEntity->getInfoboxValue($key))) {
                    continue;
                }
                if (($sw = $this->GetStringWidth($this->documentTranslator->translate($key, 'lerp_doc'))) > $labelLength) {
                    $labelLength = $sw;
                }
            }
            foreach ($this->baseDataEntity->getInfobox() as $key => $val) {
                if (empty($val)) {
                    continue;
                }
                $this->SetX($x);
                $this->Cell($labelLength, 0, $this->documentTranslator->translate($key, 'lerp_doc'), 0, 0, 'L', false);
                $this->Cell(0, 0, ': ' . $val, 0, 1, 'L', false);
            }

            $this->SetXY($x, 92 - 5);
            $this->SetFont($this->fontFamilyDefault, 'B');
            $this->Cell(70, 0, $this->documentTranslator->translate('date', 'lerp_doc') . ': ' . date('d.m.Y'), 0, 1, 'L', false);
        }
    }

    protected int $row7_c1_w = 10;
    protected int $row7_c2_w = 10;
    protected int $row7_c3_w = 10;
    protected int $row7_c4_w = 20;
    protected int $row7_c5_w = 70;
    protected int $row7_c6_w = 25;
    protected int $row7_c7_w = 25;
    protected int $row7_c6_x = 0;
    protected int $row7_c6_y = 0;
    protected int $row7_c1_y = 0;

    public function Row7header(string $id, string $quantity, string $quantityUnit, string $productNo, string $productText, string $price, string $priceSum): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->Cell($this->row7_c1_w, 0, $id, 0, 0, 'L', true);
        $this->Cell($this->row7_c2_w, 0, $quantity, 0, 0, 'L', true);
        $this->Cell($this->row7_c3_w, 0, $quantityUnit, 0, 0, 'L', true);
        $this->Cell($this->row7_c4_w, 0, $productNo, 0, 0, 'L', true);
        $this->Cell($this->row7_c5_w, 0, $productText, 0, 0, 'L', true);
        $this->row7_c6_x = $this->GetX();
        $this->Cell($this->row7_c6_w, 0, $price, 0, 0, 'R', true);
        $this->Cell($this->row7_c7_w, 0, $priceSum, 0, 1, 'R', true);
        $this->row7_c1_y = $this->GetY();
    }

    public function Row7(string $id, string $quantity, string $quantityUnit, string $productNo, string $productText, string $price, string $priceSum): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->row7_c1_y);
        $this->Cell($this->row7_c1_w, 0, $id, 0, 0, 'L', false);
        $this->Cell($this->row7_c2_w, 0, $quantity, 0, 0, 'L', false);
        $this->Cell($this->row7_c3_w, 0, $quantityUnit, 0, 0, 'L', false);
        $this->Cell($this->row7_c4_w, 0, $productNo, 0, 0, 'L', false);
        $this->row7_c6_y = $this->GetY();
        $this->MultiCell($this->row7_c5_w, 0, html_entity_decode($productText), 0, 'L', false, 1);
        $this->row7_c1_y = $this->GetY();
        $this->SetXY($this->row7_c6_x, $this->row7_c6_y);
        $this->Cell($this->row7_c6_w, 0, $price, 0, 0, 'R', false);
        $this->Cell($this->row7_c7_w, 0, $priceSum, 0, 1, 'R', false);
    }

    public function Row7sum(string $label, string $value): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->SetY($this->row7_c1_y);
        $this->Cell($this->row7_c1_w + $this->row7_c2_w + $this->row7_c3_w + $this->row7_c4_w + $this->row7_c5_w, 0, '', 0, 0, 'L', false);
        $this->Cell($this->row7_c6_w, 0, $label, 'T', 0, 'L', false);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->Cell($this->row7_c7_w, 0, $value, 'T', 1, 'R', false);
        $this->row7_c1_y = $this->GetY();
    }

    protected int $row6_c1_w = 13;
    protected int $row6_c2_w = 13;
    protected int $row6_c3_w = 20;
    protected int $row6_c4_w = 74;
    protected int $row6_c5_w = 25;
    protected int $row6_c6_w = 25;
    protected int $row6_c5_x = 0;
    protected int $row6_c5_y = 0;
    protected int $row6_c1_y = 0;

    public function Row6header(string $quantity, string $quantityUnit, string $productNo, string $productText, string $price, string $priceSum): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->Cell($this->row6_c1_w, 0, $quantity, 0, 0, 'L', true);
        $this->Cell($this->row6_c2_w, 0, $quantityUnit, 0, 0, 'L', true);
        $this->Cell($this->row6_c3_w, 0, $productNo, 0, 0, 'L', true);
        $this->Cell($this->row6_c4_w, 0, $productText, 0, 0, 'L', true);
        $this->row6_c5_x = $this->GetX();
        $this->Cell($this->row6_c5_w, 0, $price, 0, 0, 'R', true);
        $this->Cell($this->row6_c6_w, 0, $priceSum, 0, 1, 'R', true);
        $this->row6_c1_y = $this->GetY();
    }

    public function Row6(string $quantity, string $quantityUnit, string $productNo, string $productText, string $price, string $priceSum): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->row6_c1_y);
        $this->Cell($this->row6_c1_w, 0, $quantity, 0, 0, 'L', false);
        $this->Cell($this->row6_c2_w, 0, $quantityUnit, 0, 0, 'L', false);
        $this->Cell($this->row6_c3_w, 0, $productNo, 0, 0, 'L', false);
        $this->row6_c5_y = $this->GetY();
        $this->MultiCell($this->row6_c4_w, 0, html_entity_decode($productText), 0, 'L', false, 1);
        $this->row6_c1_y = $this->GetY();
        $this->SetXY($this->row6_c5_x, $this->row6_c5_y);
        $this->Cell($this->row6_c5_w, 0, $price, 0, 0, 'R', false);
        $this->Cell($this->row6_c6_w, 0, $priceSum, 0, 1, 'R', false);
    }

    public function Row6sum(string $label, string $value): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->SetY($this->row6_c1_y);
        $this->Cell($this->row6_c1_w + $this->row6_c2_w + $this->row6_c3_w + $this->row6_c4_w, 0, '', 0, 0, 'L', false);
        $this->Cell($this->row6_c5_w, 0, $label, 'T', 0, 'L', false);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->Cell($this->row6_c6_w, 0, $value, 'T', 1, 'R', false);
        $this->row6_c1_y = $this->GetY();
    }

    protected int $row4_c1_w = 13;
    protected int $row4_c2_w = 13;
    protected int $row4_c3_w = 20;
    protected int $row4_c4_w = 124;
    protected int $row4_c1_y = 0;

    public function Row4header(string $quantity, string $quantityUnit, string $productNo, string $productText): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->Cell($this->row4_c1_w, 0, $quantity, 0, 0, 'L', true);
        $this->Cell($this->row4_c2_w, 0, $quantityUnit, 0, 0, 'L', true);
        $this->Cell($this->row4_c3_w, 0, $productNo, 0, 0, 'L', true);
        $this->Cell($this->row4_c4_w, 0, $productText, 0, 1, 'L', true);
        $this->row4_c1_y = $this->GetY();
    }

    public function Row4(string $quantity, string $quantityUnit, string $productNo, string $productText): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->row4_c1_y);
        $this->Cell($this->row4_c1_w, 0, $quantity, 0, 0, 'L', false);
        $this->Cell($this->row4_c2_w, 0, $quantityUnit, 0, 0, 'L', false);
        $this->Cell($this->row4_c3_w, 0, $productNo, 0, 0, 'L', false);
        $this->MultiCell($this->row4_c4_w, 0, html_entity_decode($productText), 0, 'L', false, 1);
        $this->row4_c1_y = $this->GetY();
    }

    protected int $row5_c1_w = 10;
    protected int $row5_c2_w = 10;
    protected int $row5_c3_w = 10;
    protected int $row5_c4_w = 20;
    protected int $row5_c5_w = 120;
    protected int $row5_c1_y = 0;

    public function Row5header(string $id, string $quantity, string $quantityUnit, string $productNo, string $productText): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->Cell($this->row5_c1_w, 0, $id, 0, 0, 'L', true);
        $this->Cell($this->row5_c2_w, 0, $quantity, 0, 0, 'L', true);
        $this->Cell($this->row5_c3_w, 0, $quantityUnit, 0, 0, 'L', true);
        $this->Cell($this->row5_c4_w, 0, $productNo, 0, 0, 'L', true);
        $this->Cell($this->row5_c5_w, 0, $productText, 0, 1, 'L', true);
        $this->row5_c1_y = $this->GetY();
    }

    public function Row5(string $id, string $quantity, string $quantityUnit, string $productNo, string $productText): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->row5_c1_y);
        $this->Cell($this->row5_c1_w, 0, $id, 0, 0, 'L', false);
        $this->Cell($this->row5_c2_w, 0, $quantity, 0, 0, 'L', false);
        $this->Cell($this->row5_c3_w, 0, $quantityUnit, 0, 0, 'L', false);
        $this->Cell($this->row5_c4_w, 0, $productNo, 0, 0, 'L', false);
        $this->MultiCell($this->row5_c5_w, 0, html_entity_decode($productText), 0, 'L', false, 1);
        $this->row4_c1_y = $this->GetY();
    }
}
