<?php

namespace Lerp\DocumentTcpdf\Pdf;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;

abstract class AbstractDocumentProvider implements DocumentProviderInterface
{
    protected string $pdfFilename = '';
    protected float $priceSum = 0;
    protected float $taxSum = 0;
    protected DocumentService $documentService;
    protected Translator $documentTranslator;
    protected NumberFormatService $numberFormatService;
    protected BaseDataEntity $baseDataEntity;
    protected string $filepathPdf = '';

    public function getPdfFilename(): string
    {
        return $this->pdfFilename;
    }

    public function getPriceSum(): float
    {
        return $this->priceSum;
    }

    public function getTaxSum(): float
    {
        return $this->taxSum;
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setDocumentTranslator(Translator $documentTranslator): void
    {
        $this->documentTranslator = $documentTranslator;
    }

    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void
    {
        $this->baseDataEntity = $baseDataEntity;
    }

}
