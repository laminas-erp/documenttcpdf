<?php

namespace Lerp\DocumentTcpdf\Pdf\Offer;

use Lerp\Document\Pdf\Concrete\OfferOfferProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\Order\Entity\Offer\OfferItemEntity;

abstract class AbstractPdfOffer extends AbstractDocumentProvider implements OfferOfferProviderInterface
{
    protected string $docOfferNoCompl;

    public function setDocOfferNoCompl(string $docOfferNoCompl): void
    {
        $this->docOfferNoCompl = $docOfferNoCompl;
    }

    /**
     * @var OfferItemEntity[]
     */
    protected array $offerItemEntities;

    /**
     * @return OfferItemEntity[]
     */
    public function getOfferItemEntities(): array
    {
        return $this->offerItemEntities;
    }

    /**
     * @param array $offerItemEntities
     */
    public function setOfferItemEntities(array $offerItemEntities): void
    {
        $this->offerItemEntities = $offerItemEntities;
    }

}
