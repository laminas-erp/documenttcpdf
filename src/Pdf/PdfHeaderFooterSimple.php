<?php

namespace Lerp\DocumentTcpdf\Pdf;

use Bitkorn\Images\Tools\Svg\SvgAspectRatio;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Lerp\Document\Pdf\DocumentSimpleProviderInterface;
use Lerp\Document\Service\DocumentService;
use Picqer\Barcode\BarcodeGenerator;
use Picqer\Barcode\BarcodeGeneratorSVG;

class PdfHeaderFooterSimple extends PdfClass implements DocumentSimpleProviderInterface
{
    protected string $pdfFilename = '';
    protected DocumentService $documentService;
    protected string $lang = 'de';
    protected NumberFormatService $numberFormatService;

    protected string $filepathPdf = '';

    protected string $protectPasswd = '';
    protected string $fontFamilyDefault = 'helvetica';
    protected int $footerPositionFromBottom = -25;
    protected SvgAspectRatio $svgAspectRatio;

    protected int $positionTopmost = 6;
    protected int $contentStart = 26;
    protected $creator = 'Aviation ERP';

    public function getPdfFilename(): string
    {
        return $this->pdfFilename;
    }

    /**
     * @var array From config
     */
    protected array $colorArrayBrand = [];

    /**
     * @var string From config
     */
    protected string $pathLogoSimple = '';

    protected int $logoSimpleWidth = 0;
    protected int $logoSimpleHeight = 12; // fix

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setLangIso(string $lang): void
    {
        if (isset($this->documentTranslator)) {
            $this->documentTranslator->setLocale($lang);
        }
        $this->lang = $lang;
    }

    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    public function makeDocument(): void
    {
        $this->makeBasics();
    }

    /**
     * @return string
     * After the call of this function, $this holds ONLY members from base class TCPDF (all other are lost)!
     */
    public function writeDocument(): string
    {
        $this->Output($p = $this->filepathPdf, 'F');
        /*
         * echt krank: laut debugger ist man ab hier immernoch in PdfOrderConfirm.
         * Es existieren aber keine Klassenvariablen aus PdfClass, PdfHeaderFooterBrand, PdfCorrespondenceDefault oder PdfOrderConfirm.
         * Es existieren nur Klassenvariablen aus TCPDF
         */
        return $p;
    }

    public function setProtectPasswd(string $protectPasswd): void
    {
        $this->protectPasswd = $protectPasswd;
    }

    public function setColorArrayBrand(array $colorArrayBrand): void
    {
        $this->colorArrayBrand = $colorArrayBrand;
    }

    public function setPathLogo(string $pathLogo): void
    {
        $this->pathLogo = $pathLogo;
    }

    public function setPathBrand(string $pathBrand): void
    {
        $this->pathBrand = $pathBrand;
    }

    public function setPathLogoSimple(string $pathLogoSimple): void
    {
        $this->pathLogoSimple = $pathLogoSimple;
    }

    /**
     * 1. compute images width with aspect ratio (keep height)
     * 2. set PDF protection password
     * 3. set PDF meta data
     * 4. set color and font
     */
    public function makeBasics()
    {
        $this->svgAspectRatio = new SvgAspectRatio();
        if (!$this->svgAspectRatio->loadSvg($this->storageLocationImg . $this->pathLogoSimple)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->storageLocationImg . $this->pathLogoSimple);
        }
        $this->logoSimpleWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->logoSimpleHeight;

        $this->SetProtection(['modify'], null, $this->protectPasswd);
        // PDF basics
        $this->SetCreator($this->creator);
        $this->SetAuthor($this->author);
        $this->SetTitle($this->title);
        $this->SetSubject($this->subject);
        $this->SetMargins($this->marginLeft, $this->marginTop, $this->marginRight); // left, top, right
        $this->SetAutoPageBreak(true, $this->marginBottom + (-1 * $this->footerPositionFromBottom));

        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->SetFontSize($this->fontSizeInitial);
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);
        $this->SetY($this->contentStart);
    }

    public function Header()
    {
        $this->SetFontSize($this->fontSizeInitial);
        $this->ImageSVG($this->storageLocationImg . $this->pathLogoSimple, ($this->getPageWidth() / 2) - ($this->logoSimpleWidth / 2), $this->positionTopmost, null, $this->logoSimpleHeight);
        $this->SetY($this->positionTopmost + $this->logoSimpleHeight + 1);
        $this->Cell($this->getContentWidth(), 1, '', 'T', 1, 'C', false);
        $this->contentStart = $this->positionTopmost + $this->logoSimpleHeight + 1;
    }

    public function Footer()
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_XXS);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->getPageHeight() - 6);
        $this->Cell($this->getContentWidth(), 0, $this->documentTranslator->translate('page', 'lerp_doc') . ' ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'C');
    }

    protected BarcodeGeneratorSVG $barcodeSvg;

    protected function getBarcodeSvg(string $chars): string
    {
        if (!isset($this->barcodeSvg)) {
            $this->barcodeSvg = new BarcodeGeneratorSVG();
        }
        return $this->barcodeSvg->getBarcode(str_pad($chars, 10, "0", STR_PAD_LEFT), BarcodeGenerator::TYPE_INTERLEAVED_2_5, 2, 30);
    }
}
