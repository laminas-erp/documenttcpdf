<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Pdf\Concrete\EstimateProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\DocumentTcpdf\Traits\OrderTrait;

abstract class AbstractPdfEstimate extends AbstractDocumentProvider implements EstimateProviderInterface
{
    use OrderTrait;

    protected string $docEstimateNoCompl;

    public function setDocEstimateNoCompl(string $docEstimateNoCompl): void
    {
        $this->docEstimateNoCompl = $docEstimateNoCompl;
    }
}
