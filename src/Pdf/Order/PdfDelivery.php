<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\PdfClass;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;
use Lerp\Order\Entity\Order\OrderItemEntity;

class PdfDelivery extends AbstractPdfDelivery
{
    protected PdfCorrespondenceDefault $pdf;

    public function setPdf(PdfCorrespondenceDefault $pdf): void
    {
        $this->pdf = $pdf;
    }

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void
    {
        $this->baseDataEntity = $baseDataEntity;
        $this->pdf->setBaseDataEntity($this->baseDataEntity);
    }

    public function makeDocument(): void
    {
        if (empty($this->orderItemEntities)) {
            return;
        }
        $this->pdf->makeBasics();
        /** @var OrderItemEntity $orderItem */
        $orderItem = $this->orderItemEntities[0];
        $this->pdfFilename = time() . '_' . DocumentService::FILENAME_ORDER_DELIVERY;
        $this->filepathPdf = $this->documentService->computeDocumentFolder(
                $this->pdf->getStorageLocationPdf(),
                DocumentService::DOC_TYPE_ORDER_DELIVERY,
                floor($orderItem->getOrderTimeCreateUnix()),
                $orderItem->getOrderUuid()
            ) . DIRECTORY_SEPARATOR . $this->pdfFilename;
        $this->baseDataEntity->setInfoboxOrderNo($orderItem->getOrderNoCompl());

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), 'B');
        $this->pdf->AddPage('P');
        $this->pdf->Cell(25, 0, 'Lieferschein Nr. ' . $this->docDeliveryNoCompl, 0, 1, 'L', false);

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->Ln();
        $this->pdf->Cell(25, 0, $this->baseDataEntity->getSalutation() . ',', 0, 1, 'L', false);
        $this->pdf->Ln();
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentStartHTML(), 0, 1);
        $this->pdf->Ln();

        $this->pdf->Row4header(
            $this->documentTranslator->translate('th_amount', 'lerp_doc')
            , $this->documentTranslator->translate('th_quantityunit', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_no', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_desc', 'lerp_doc')
        );

        foreach ($this->orderItemEntities as $item) {
            $this->pdf->Row4(
                $this->numberFormatService->format($item->getOrderItemQuantity()),
                $item->getQuantityunitLabel(),
                $item->getProductNoNo(),
                $item->getOrderItemTextShort());
        }
        $this->pdf->Ln();

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentEndHTML(), 0, 1);
    }

    /**
     * @return string
     * After the call of this function, $this holds ONLY members from base class TCPDF (all other are lost)!
     */
    public function writeDocument(): string
    {
        $this->pdf->Output($p = $this->filepathPdf, 'F');
        /*
         * echt krank: laut debugger ist man ab hier immernoch in PdfOrderConfirm.
         * Es existieren aber keine Klassenvariablen aus PdfClass, PdfHeaderFooterBrand, PdfCorrespondenceDefault oder PdfOrderConfirm.
         * Es existieren nur Klassenvariablen aus TCPDF
         */
        return $p;
    }
}
