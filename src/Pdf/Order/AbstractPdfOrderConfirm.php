<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Pdf\Concrete\OrderConfirmProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\DocumentTcpdf\Traits\OrderTrait;

abstract class AbstractPdfOrderConfirm extends AbstractDocumentProvider implements OrderConfirmProviderInterface
{
    use OrderTrait;

    protected string $docOrderConfirmNoCompl;

    public function setDocOrderConfirmNoCompl(string $docOrderConfirmNoCompl): void
    {
        $this->docOrderConfirmNoCompl = $docOrderConfirmNoCompl;
    }

}
