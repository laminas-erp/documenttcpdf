<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Service\DocumentService;
use Lerp\DocumentTcpdf\Pdf\PdfClass;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;
use Lerp\Order\Entity\Order\OrderItemEntity;

class PdfEstimate extends AbstractPdfEstimate
{
    protected PdfCorrespondenceDefault $pdf;

    public function setPdf(PdfCorrespondenceDefault $pdf): void
    {
        $this->pdf = $pdf;
    }

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void
    {
        $this->baseDataEntity = $baseDataEntity;
        $this->pdf->setBaseDataEntity($this->baseDataEntity);
    }

    public function makeDocument(): void
    {
        if (empty($this->orderItemEntities)) {
            return;
        }
        $this->pdf->makeBasics();
        /** @var OrderItemEntity $orderItem */
        $orderItem = $this->orderItemEntities[0];
        $this->pdfFilename = time() . '_' . DocumentService::FILENAME_ESTIMATE;
        $this->filepathPdf = $this->documentService->computeDocumentFolder(
                $this->pdf->getStorageLocationPdf(),
                DocumentService::DOC_TYPE_ESTIMATE,
                floor($orderItem->getOrderTimeCreateUnix()),
                $orderItem->getOrderUuid()
            ) . DIRECTORY_SEPARATOR . $this->pdfFilename;
        $this->baseDataEntity->setInfoboxOrderNo($orderItem->getOrderNoCompl());

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), 'B');
        $this->pdf->AddPage('P');
        $this->pdf->Cell(25, 0, $this->documentTranslator->translate('estimate_no', 'lerp_doc') . ' ' . $this->docEstimateNoCompl, 0, 1, 'L', false);

        $this->pdf->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->Ln();
        $this->pdf->Cell(25, 0, $this->baseDataEntity->getSalutation() . ',', 0, 1, 'L', false);
        $this->pdf->Ln();
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentStartHTML(), 0, 1);
        $this->pdf->Ln();

        $this->pdf->Row7header(
            $this->documentTranslator->translate('th_pos', 'lerp_doc')
            , $this->documentTranslator->translate('th_amount', 'lerp_doc')
            , $this->documentTranslator->translate('th_quantityunit', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_no', 'lerp_doc')
            , $this->documentTranslator->translate('th_product_desc', 'lerp_doc')
            , $this->documentTranslator->translate('th_price', 'lerp_doc') . ' €'
            , $this->documentTranslator->translate('th_price_sum', 'lerp_doc') . ' €'
        );

        foreach ($this->orderItemEntities as $item) {
            $itemSum = $item->getOrderItemPriceTotal();
            $itemTaxSum = $itemSum * ($item->getOrderItemTaxp() / 100);
            $this->priceSum += $itemSum;
            $this->taxSum += $itemTaxSum;
            $this->pdf->Row7(
                $item->getOrderItemId(),
                $this->numberFormatService->format($item->getOrderItemQuantity()),
                $item->getQuantityunitLabel(),
                $item->getProductNoNo(),
                $item->getOrderItemTextShort(),
                $this->numberFormatService->format($item->getOrderItemPrice()),
                $this->numberFormatService->format($itemSum));
        }
        $this->pdf->Ln();
        $this->pdf->Row7sum('Netto', $this->numberFormatService->format($this->priceSum));
        $this->pdf->Row7sum('Ums.-St.', $this->numberFormatService->format($this->taxSum));
        $this->pdf->Row7sum('Brutto', $this->numberFormatService->format($this->priceSum + $this->taxSum));

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont($this->pdf->getFontFamilyDefault(), '');
        $this->pdf->writeHTMLCell($this->pdf->getContentWidth(), 0, $this->pdf->getMarginLeft(), $this->pdf->GetY(), $this->baseDataEntity->getContentEndHTML(), 0, 1);
    }

    /**
     * @return string
     * After the call of this function, $this holds ONLY members from base class TCPDF (all other are lost)!
     */
    public function writeDocument(): string
    {
        $this->pdf->Output($p = $this->filepathPdf, 'F');
        /*
         * echt krank: laut debugger ist man ab hier immernoch in PdfOrderConfirm.
         * Es existieren aber keine Klassenvariablen aus PdfClass, PdfHeaderFooterBrand, PdfCorrespondenceDefault oder PdfOrderConfirm.
         * Es existieren nur Klassenvariablen aus TCPDF
         */
        return $p;
    }
}
