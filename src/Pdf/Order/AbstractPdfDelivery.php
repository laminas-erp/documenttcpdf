<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Pdf\Concrete\DeliveryProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\DocumentTcpdf\Traits\OrderTrait;

abstract class AbstractPdfDelivery extends AbstractDocumentProvider implements DeliveryProviderInterface
{
    use OrderTrait;

    protected string $docDeliveryNoCompl;

    public function setDocDeliveryNoCompl(string $docDeliveryNoCompl): void
    {
        $this->docDeliveryNoCompl = $docDeliveryNoCompl;
    }
}
