<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Pdf\Concrete\ProformaProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\DocumentTcpdf\Traits\OrderTrait;

abstract class AbstractPdfProforma extends AbstractDocumentProvider implements ProformaProviderInterface
{
    use OrderTrait;

    protected string $docProformaNoCompl;

    public function setDocProformaNoCompl(string $docProformaNoCompl): void
    {
        $this->docProformaNoCompl = $docProformaNoCompl;
    }
}
