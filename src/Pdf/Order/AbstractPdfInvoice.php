<?php

namespace Lerp\DocumentTcpdf\Pdf\Order;

use Lerp\Document\Pdf\Concrete\InvoiceProviderInterface;
use Lerp\DocumentTcpdf\Pdf\AbstractDocumentProvider;
use Lerp\DocumentTcpdf\Traits\OrderTrait;

abstract class AbstractPdfInvoice extends AbstractDocumentProvider implements InvoiceProviderInterface
{
    use OrderTrait;

    protected string $docInvoiceNoCompl;

    public function setDocInvoiceNoCompl(string $docInvoiceNoCompl): void
    {
        $this->docInvoiceNoCompl = $docInvoiceNoCompl;
    }
}
