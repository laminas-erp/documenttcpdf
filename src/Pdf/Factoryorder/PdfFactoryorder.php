<?php

namespace Lerp\DocumentTcpdf\Pdf\Factoryorder;

use Lerp\Document\Pdf\Concrete\FactoryorderProviderInterface;
use Lerp\Document\Service\FactoryorderDocumentService;
use Lerp\DocumentTcpdf\Pdf\PdfClass;
use Lerp\DocumentTcpdf\Pdf\PdfHeaderFooterSimple;
use Lerp\Factoryorder\Entity\FactoryorderEntity;
use Lerp\Factoryorder\Entity\FactoryorderWorkflowEntity;

class PdfFactoryorder extends PdfHeaderFooterSimple implements FactoryorderProviderInterface
{
    protected FactoryorderEntity $factoryOrder;

    /**
     * @var FactoryorderWorkflowEntity[]
     */
    protected array $factoryorderWorkflows;

    public function setFactoryOrder(FactoryorderEntity $factoryOrder): void
    {
        $this->factoryOrder = $factoryOrder;
    }

    /**
     * @param FactoryorderWorkflowEntity[] $factoryorderWorkflows
     */
    public function setFactoryorderWorkflows(array $factoryorderWorkflows): void
    {
        $this->factoryorderWorkflows = $factoryorderWorkflows;
    }

    protected int $colonFieldWidth = 28;
    protected int $numberFieldWidth = 30;

    public function makeDocument(): void
    {
        if (empty($this->factoryOrder)) {
            return;
        }
        $this->makeBasics();
        $this->pdfFilename = time() . '_' . FactoryorderDocumentService::FILENAME_FACTORYORDER;
        $this->filepathPdf = $this->documentService->computeDocumentFolder(
                $this->storageLocationPdf,
                FactoryorderDocumentService::DOC_TYPE_FACTORYORDER,
                floor($this->factoryOrder->getFactoryorderTimeCreateUnix()),
                $this->factoryOrder->getFactoryorderUuid()
            ) . DIRECTORY_SEPARATOR . $this->pdfFilename;

        $this->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->AddPage('P');
        $this->Cell(25, 0, 'Auftrag ' . $this->factoryOrder->getOrderNo(), 0, 1, 'L', false);

        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFont($this->fontFamilyDefault, '');

        $this->Cell($this->colonFieldWidth, 0, 'Produkt:', 0, 0, 'L', false);
        $this->Cell($this->numberFieldWidth, 0, $this->factoryOrder->getProductNoNo(), 0, 0, 'L', false);
        $this->ImageSVG('@' . $this->getBarcodeSvg($this->factoryOrder->getProductNoNo()), $this->GetX(), $this->GetY() + 1, 0, 3.4);
        $this->Ln();

        $this->Cell($this->colonFieldWidth, 0, '', 0, 0, 'L', false);
        $this->Cell(0, 0, $this->factoryOrder->getProductTextShort(), 0, 1, 'L', false);

        $this->Cell($this->colonFieldWidth, 0, 'Prod. Anzahl:', 0, 0, 'L', false);
        $this->Cell(0, 0, $this->factoryOrder->getFactoryorderQuantity(), 0, 1, 'L', false);

        $this->Cell($this->colonFieldWidth, 0, 'Prod. Datum:', 0, 0, 'L', false);
        $this->Cell(0, 0, substr($this->factoryOrder->getFactoryorderTimeCreate(), 0, 10), 0, 1, 'L', false);

        $this->Cell($this->colonFieldWidth, 0, 'Termin:', 0, 0, 'L', false);
        $this->Cell(0, 0, substr($this->factoryOrder->getOrderTimeFinishSchedule(), 0, 10), 0, 1, 'L', false);

        $this->Cell($this->colonFieldWidth, 0, 'BA Nr.:', 0, 0, 'L', false);
        $this->Cell($this->numberFieldWidth, 0, $this->factoryOrder->getFactoryorderNo(), 0, 0, 'L', false);
        $this->ImageSVG('@' . $this->getBarcodeSvg($this->factoryOrder->getFactoryorderNo()), $this->GetX(), $this->GetY() + 1, 0, 3.4);
        $this->Ln();

        $this->Cell($this->colonFieldWidth, 0, 'Auftrag:', 0, 0, 'L', false);
        $this->Cell($this->numberFieldWidth, 0, $this->factoryOrder->getOrderNo(), 0, 0, 'L', false);
        $this->ImageSVG('@' . $this->getBarcodeSvg($this->factoryOrder->getOrderNo()), $this->GetX(), $this->GetY() + 1, 0, 3.4);
        $this->Ln();

        $this->Cell($this->colonFieldWidth, 0, 'Kunde:', 0, 0, 'L', false);
        $this->Cell(0, 0, $this->factoryOrder->getCustomerName() . ' (' . $this->factoryOrder->getCustomerNo() . ')', 0, 1, 'L', false);

        $this->Cell($this->colonFieldWidth, 0, 'Ansprechp.:', 0, 0, 'L', false);
        $this->Cell(0, 0, $this->factoryOrder->getUserDetailsNameFirst() . ' ' . $this->factoryOrder->getUserDetailsNameLast(), 0, 1, 'L', false);

        /*
         * boxes
         */
        $this->Ln();
        $this->Ln();

        $this->HeadingBorderBox('Artikeldaten', $this->factoryOrder->getProductTextLong());
        $y1 = $this->GetY();
        $this->Ln();
        $cellHeight = $this->GetY() - $y1;
        $this->HeadingBorderBox('Arbeitsanweisungen', $this->factoryOrder->getFactoryorderBriefing());
        $this->Ln($cellHeight); // because cell/ln heigth is the last height, we must set the pre-last :)

        $this->RowWorkflowHeader();
        foreach ($this->factoryorderWorkflows as $workflow) {
            if (!empty($c = $workflow->getWorkflowCode())) {
                $this->RowWorkflow($workflow->getFactoryorderWorkflowText(), $c);
                $this->ImageSVG('@' . $this->getBarcodeSvg($c), $this->row4_c2_x + 8, $this->row4_c1_y + $this->row4_h / 3, 0, 3.4); // $this->row4_c2_w
            } else {
                $this->RowWorkflow($workflow->getFactoryorderWorkflowText(), '');
            }
        }
        $this->Ln();
    }

    /**
     * Creates a box with heading (background color) and content.
     * @param string $heading
     * @param string $content
     */
    protected function HeadingBorderBox(string $heading, string $content): void
    {
        $this->SetFont('', 'B');
        $this->SetFillColorArray($this->colorArrayGreyLight);
        $this->Cell($this->getContentWidth(), 0, $heading, 'LTR', 0, 'L', true);
        $this->Ln();
        $this->SetFont('', '');
        $this->MultiCell($this->getContentWidth(), 0, $content, 'LRB', 'L', false);
    }

    protected int $row4_h = 14;
    protected int $row4_c1_w = 50;
    protected int $row4_c2_w = 30;
    protected int $row4_c3_w = 50;
    protected int $row4_c4_w = 0;
    protected int $row4_col1_y = 0;
    protected int $row4_c1_y = 0;
    protected int $row4_c2_x = 0;

    protected function RowWorkflowHeader(): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFillColorArray($this->colorArrayGreyLight);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->Cell($this->row4_c1_w, 0, 'Arbeitsgang', 'LTB', 0, 'L', true);
        $this->Cell($this->row4_c2_w, 0, 'Code', 'TB', 0, 'L', true);
        $this->Cell($this->row4_c3_w, 0, 'Bearbeiter', 'LTB', 0, 'L', true);
        $this->Cell($this->row4_c4_w, 0, 'Prüfer', 'LTRB', 1, 'L', true);
        $this->row4_col1_y = $this->GetY();
    }

    protected function RowWorkflow(string $workflow, string $code): void
    {
        $this->row4_c1_y = $this->GetY();
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->row4_col1_y);
        $this->MultiCell($this->row4_c1_w, $this->row4_h, substr($workflow, 0, 80), 'LB', 'L', false, 0);
        $this->row4_c2_x = $this->GetX(); // barcode position
        $this->Cell($this->row4_c2_w, $this->row4_h, $code, 'LB', 0, 'L', false);
        $this->Cell($this->row4_c3_w, $this->row4_h, ' ', 'LB', 0, 'L', false);
        $this->MultiCell($this->row4_c4_w, $this->row4_h, ' ', 'LBR', 'L', false, 1);
        $this->row4_col1_y = $this->GetY();
    }
}
