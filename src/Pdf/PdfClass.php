<?php

namespace Lerp\DocumentTcpdf\Pdf;

use Laminas\I18n\Translator\Translator;

/**
 * Class PdfClass
 * @package Lerp\DocumentTcpdf\Pdf
 *
 * Das ist die basis Klasse Aller.
 * Weitere Vererbungsschichten ergeben sich aus Gemeinsamkeiten der Header und Footer.
 */
class PdfClass extends \TCPDF
{
    protected string $storageLocationTmp;
    protected string $storageLocationImg;
    protected string $storageLocationPdf;

    protected int $marginLeft = 25; // DIN 5008 = 25
    protected int $marginTop = 20;
    protected int $marginRight = 15;
    protected int $marginBottom = 20;
    protected int $marginBottomAddPage = 20;
    protected int $marginBottomAddPageMulti = 10;

    const FONT_SIZE_XXS = 6;
    const FONT_SIZE_XS = 8;
    const FONT_SIZE_S = 10;
    const FONT_SIZE_M = 12;
    const FONT_SIZE_L = 14;
    const FONT_SIZE_XL = 16;
    const FONT_SIZE_XXL = 18;

    public int $fontSizeInitial = self::FONT_SIZE_M;

    public array $colorArrayWhite = [255, 255, 255];
    public array $colorArrayBlack = [10, 10, 10];
    public array $colorArrayGreyLight = [200, 200, 200];
    protected Translator $documentTranslator;

    public function setStorageLocationTmp(string $storageLocationTmp): void
    {
        $this->storageLocationTmp = $storageLocationTmp;
    }

    public function setStorageLocationImg(string $storageLocationImg): void
    {
        $this->storageLocationImg = $storageLocationImg;
    }

    public function setStorageLocationPdf(string $storageLocationPdf): void
    {
        $this->storageLocationPdf = $storageLocationPdf;
    }

    public function setDocumentTranslator(Translator $documentTranslator): void
    {
        $this->documentTranslator = $documentTranslator;
    }

    public function getStorageLocationTmp(): string
    {
        return $this->storageLocationTmp;
    }

    public function getStorageLocationImg(): string
    {
        return $this->storageLocationImg;
    }

    public function getStorageLocationPdf(): string
    {
        return $this->storageLocationPdf;
    }

    public function getMarginLeft(): int
    {
        return $this->marginLeft;
    }

    public function getMarginTop(): int
    {
        return $this->marginTop;
    }

    public function getMarginRight(): int
    {
        return $this->marginRight;
    }

    public function getMarginBottom(): int
    {
        return $this->marginBottom;
    }

    public function getMarginBottomAddPage(): int
    {
        return $this->marginBottomAddPage;
    }

    public function getMarginBottomAddPageMulti(): int
    {
        return $this->marginBottomAddPageMulti;
    }

    public function getFontSizeInitial(): int
    {
        return $this->fontSizeInitial;
    }

    public function getColorArrayWhite(): array
    {
        return $this->colorArrayWhite;
    }

    public function getColorArrayBlack(): array
    {
        return $this->colorArrayBlack;
    }

    public function getColorArrayGreyLight(): array
    {
        return $this->colorArrayGreyLight;
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);

        /*
         * Falzmarke - https://de.wikipedia.org/wiki/Falzmarke
         * Form A = Briefblatt mit 27 mm hohem Briefkopf
         * 1. 87 mm
         * 2. 192 mm
         * Form B = Briefblatt mit 45 mm hohem Briefkopf
         * 1. 105 mm
         * 2. 210 mm
         */
        $this->setImageScale(1.25);
        $this->Image($this->storageLocationImg . '/falzmarke.svg', 3, 87, 4, 0.1, 'svg', '', '', false, 300, '', false, false, 0);
        $this->Image($this->storageLocationImg . '/falzmarke.svg', 3, 192, 4, 0.1, 'svg', '', '', false, 300, '', false, false, 0);
        /**
         * Lochmarke
         */
        $this->Image($this->storageLocationImg . '/lochmarke.svg', 3, 148.5, 6, 0.1, 'svg', '', '', false, 300, '', false, false, 0);

    }

    public function getContentWidth(): int
    {
        return $this->getPageWidth() - ($this->marginLeft + $this->marginRight);
    }
}
