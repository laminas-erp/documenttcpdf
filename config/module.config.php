<?php

namespace Lerp\DocumentTcpdf;

use Lerp\Document\Pdf\Concrete\DeliveryProviderInterface;
use Lerp\Document\Pdf\Concrete\EstimateProviderInterface;
use Lerp\Document\Pdf\Concrete\FactoryorderProviderInterface;
use Lerp\Document\Pdf\Concrete\InvoiceProviderInterface;
use Lerp\Document\Pdf\Concrete\OfferOfferProviderInterface;
use Lerp\Document\Pdf\Concrete\OrderConfirmProviderInterface;
use Lerp\Document\Pdf\Concrete\ProformaProviderInterface;
use Lerp\Document\Pdf\Concrete\PurchaseDeliverProviderInterface;
use Lerp\Document\Pdf\Concrete\PurchaseOrderProviderInterface;
use Lerp\Document\Pdf\Concrete\PurchaseRequestProviderInterface;
use Lerp\Document\Unique\UniqueNumberProviderInterface;
use Lerp\DocumentTcpdf\Factory\Pdf\Factoryorder\PdfFactoryorderFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Offer\PdfOfferFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Order\PdfDeliveryFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Order\PdfEstimateFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Order\PdfInvoiceFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Order\PdfOrderConfirmFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Order\PdfProformaFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\PdfCorrespondenceDefaultFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\PdfHeaderFooterBrandFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Purchase\PdfPurchaseDeliverFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Purchase\PdfPurchaseOrderFactory;
use Lerp\DocumentTcpdf\Factory\Pdf\Purchase\PdfPurchaseRequestFactory;
use Lerp\DocumentTcpdf\Factory\Unique\UniqueNumberProviderFactory;
use Lerp\DocumentTcpdf\Factory\View\FooterHtmlFactory;
use Lerp\DocumentTcpdf\Pdf\PdfCorrespondenceDefault;
use Lerp\DocumentTcpdf\Pdf\PdfHeaderFooterBrand;
use Lerp\DocumentTcpdf\View\FooterHtml;

return [
    'service_manager'     => [
        'factories'  => [
            PdfHeaderFooterBrand::class             => PdfHeaderFooterBrandFactory::class,
            PdfCorrespondenceDefault::class         => PdfCorrespondenceDefaultFactory::class,
            //
            PurchaseOrderProviderInterface::class   => PdfPurchaseOrderFactory::class,
            PurchaseDeliverProviderInterface::class => PdfPurchaseDeliverFactory::class,
            PurchaseRequestProviderInterface::class => PdfPurchaseRequestFactory::class,
            EstimateProviderInterface::class        => PdfEstimateFactory::class,
            OrderConfirmProviderInterface::class    => PdfOrderConfirmFactory::class,
            DeliveryProviderInterface::class        => PdfDeliveryFactory::class,
            InvoiceProviderInterface::class         => PdfInvoiceFactory::class,
            FactoryorderProviderInterface::class    => PdfFactoryorderFactory::class,
            OfferOfferProviderInterface::class      => PdfOfferFactory::class,
            ProformaProviderInterface::class        => PdfProformaFactory::class,
            // unique
            UniqueNumberProviderInterface::class    => UniqueNumberProviderFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'        => [
        'aliases'    => [
        ],
        'factories'  => [
            FooterHtml::class => FooterHtmlFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'view_manager'        => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map'        => [
            'lerpDocumentTcpdf/footerHtml' => __DIR__ . '/../view/template/footer.phtml',
        ],
    ],
    'lerp_document_tcpdf' => [
        'protect_passwd'      => 'testtext',
        'path_tmp'            => __DIR__ . '/../../../../data/tmp',
        'path_img'            => __DIR__ . '/../../../../data/files/documents/img',
        'brand_color'         => '#ff4814',
        'brand_rgb'           => [255, 72, 20],
        'image_paths'         => [ // within 'path_img'
            'logo'        => '/gomolzig_logo.svg',
            'brand'       => '/document_brand.svg',
            'logo_simple' => '/gomolzig_logo_minimal_color.svg',
        ],
        'data_author'         => 'Gomolzig Aircraft Services GmbH',
        'address_line_origin' => 'Gomolzig GmbH - Postfach 466 - 58317',
    ]
];
