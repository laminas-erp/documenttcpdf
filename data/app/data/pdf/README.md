folder structure for PDF documents: /type/Y/m/d/uuid

Where 'type' ist one of the following:
- requestPurchaseOrder
- purchaseOrder
- deliveryPurchaseOrder
- offer (Angebot)
- offerCalculation (kostenvoranschlag)
- orderCalculation (kostenvoranschlag)
- orderAccept (Auftragsbestätigung)
- invoice
- deliveryOrder
- factoryOrder