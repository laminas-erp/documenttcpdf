# Laminas Module lerp/documenttcpdf

**features**
- create PDFs for lerp/document with TCPDF
- create the document numbers

Overwrite the module to have your own PDF layout and document numbers.

## tec info
- images always as SVG
